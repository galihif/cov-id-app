import React, {Component} from 'react'
import { View, ImageBackground, StyleSheet, Image, Text, TextInput, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component{
    render(){
        return(
            <ImageBackground style={{flex:1}} source={require('./img/Home.png')}>
                <View style={{flexDirection:'row', justifyContent:'flex-start'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                        <Icon name='menu' size={45} color='#686565' style={styles.menuIcon}/>
                    </TouchableOpacity>
                </View>

                <View style={{alignItems:'center', justifyContent:'center',flex:1}}>
                    <ActivityIndicator color='grey'/>
                </View>
            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    menuIcon:{
        marginTop: 30,
        marginLeft: 20
    },
})