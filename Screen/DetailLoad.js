import React, {Component} from 'react'
import { View, ImageBackground, StyleSheet, Image, Text, TextInput, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component{
    render(){
        return(
            <ImageBackground style={{flex:1}} source={require('./img/Details.png')}>
                <View style={{flexDirection:'row', justifyContent:'flex-start'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                        <Icon name='menu' size={45} color='#686565' style={styles.menuIcon}/>
                    </TouchableOpacity>
                </View>

                <Text style={styles.titleText1}>COVID CASES IN</Text>
                <Text style={styles.titleText2}>INDONESIAN PROVINCE</Text>

                <View style={{alignItems:'center', justifyContent:'center',flex:1}}>
                    <ActivityIndicator color='grey'/>
                </View>
            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    menuIcon:{
        marginTop: 30,
        marginLeft: 20
    },
    titleText1: {
        fontSize: 28,
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#686565'
    },
    titleText2: {
        fontSize: 19,
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#686565',
        marginTop: -10
    },
})